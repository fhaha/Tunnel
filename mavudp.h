#pragma once

#include <GL/glew.h>
extern GLfloat ginnerwidth ;
extern GLfloat goutterwidth;
extern GLfloat ginnerheight;
extern GLfloat goutterheight ;
extern GLboolean gFrontShow;
extern GLboolean gBackShow;
extern GLboolean gLeftShow;
extern GLboolean gRightShow;

void fourReshape();
void calc_inner_outter_rect(float innerwidth,float innerheight,float outterwidth,float outterheight);
void calc_front_params(float fh,float fw,float fho,float fwo,float outterwidth,float innerwidth,float outterheight,float innerheight);
void calc_back_params(float bh,float bw,float bho,float bwo,float outterwidth,float innerwidth,float outterheight,float innerheight);
void calc_left_params(float lh,float lw,float lho,float lwo,float outterwidth,float innerwidth,float outterheight,float innerheight);
void calc_right_params(float rh,float rw,float rho,float rwo,float outterwidth,float innerwidth,float outterheight,float innerheight);
void postDisplay();

