// Tunnel.cpp
// Demonstrates mipmapping and using texture objects
// OpenGL SuperBible
// Richard S. Wright Jr.
#include <GLTools.h>
#include <GLShaderManager.h>
#include <GLFrustum.h>
#include <GLBatch.h>
#include <GLFrame.h>
#include <GLMatrixStack.h>
#include <GLGeometryTransform.h>
#include "camera4/mavlink.h"
#include <math.h>
#include <Windows.h>

#ifdef __APPLE__
#include <glut/glut.h>
#else
#define FREEGLUT_STATIC
#include <GL/glut.h>
#endif

HANDLE g_hMutex;
bool needReshape =false;

GLShaderManager		shaderManager;			// Shader Manager
GLMatrixStack		modelViewMatrix;		// Modelview Matrix
GLMatrixStack		projectionMatrix;		// Projection Matrix
GLFrustum			viewFrustum;			// View Frustum
GLGeometryTransform	transformPipeline;		// Geometry Transform Pipeline

GLBatch             backBatch;
GLBatch             frontBatch;
GLBatch             leftBatch;
GLBatch             rightBatch;
GLBatch				carBatch;

GLfloat             viewZ = -12.0f;

#define ZV	0	// Z Value
#define XO	(16/2)	// X Out Width
#define XI	(8/2)	// X Inner Width
#define YO	(20/2)	//	Y Out Height
#define YI	(10/2)	// Y Inner Height

GLfloat TCs[4][4][2]={{{0.0f,0.3f},{0.25f,0.0f},{1.0f,0.3f},{0.75f,0.0f}},
{{0.2f,0.7f},{0.8f,0.7f},{0.3f,0.95},{0.7f,0.95f}},
{{0.0f,1.0f},{0.0f,0.0f},{1.0f,1.0f},{1.0f,0.0f}},
{{0.0f,1.0f},{0.0f,0.0f},{1.0f,1.0f},{1.0f,0.0f}}};
GLfloat carTC[4][2] = {{0.3f,0.1f},{0.7f,0.1f},{0.3f,0.9f},{0.7f,0.9f}};

GLfloat PTs[8][3] ={{-XO,YO,ZV},	//Points
					{XO,YO,ZV},
					{-XO,-YO,ZV},
					{XO,-YO,ZV},
					{-XI,YI,ZV},
					{XI,YI,ZV},
					{-XI,-YI,ZV},
					{XI,-YI,ZV}
					};

GLfloat ginnerwidth = XI * 2;
GLfloat goutterwidth = XO * 2;
GLfloat ginnerheight = YI * 2;
GLfloat goutterheight = YO * 2;

GLboolean gFrontShow = true;
GLboolean gBackShow = true;
GLboolean gLeftShow = true;
GLboolean gRightShow = true;

void calc_inner_outter_rect(float innerwidth,float innerheight,float outterwidth,float outterheight)
{
	ginnerwidth = innerwidth;
	goutterwidth = outterwidth;
	ginnerheight = innerheight;
	goutterheight = outterheight;
	outterwidth/=2;
	outterheight/=2;
	innerheight/=2;
	innerwidth/=2;
	PTs[0][0]=-outterwidth;
	PTs[0][1]=outterheight;

	PTs[1][0]=outterwidth;
	PTs[1][1]=outterheight;

	PTs[2][0]=-outterwidth;
	PTs[2][1]=-outterheight;

	PTs[3][0]=outterwidth;
	PTs[3][1]=-outterheight;

	PTs[4][0]=-innerwidth;
	PTs[4][1]=innerheight;

	PTs[5][0]=innerwidth;
	PTs[5][1]=innerheight;

	PTs[6][0]=-innerwidth;
	PTs[6][1] = -innerheight;

	PTs[7][0]=innerwidth;
	PTs[7][1]=-innerheight;
}

//parameters are texture coordination except last two
void calc_front_params(float fh,float fw,float fho,float fwo,float outterwidth,float innerwidth,float outterheight,float innerheight)
{
	float ybase = fho;	//y base line
	//float alpha = atan(fh/fw);	//y/x angle
	float xlo =fwo;	//left outter x
	float xiw = fw * innerwidth / outterwidth;	//x inner width
	float xwdiff = (fw - xiw)/2;	//x width diff between inner and outter
	
	float fhc = (goutterheight - ginnerheight) * xwdiff /(goutterwidth - ginnerheight);
	float yupline = fho + fhc / fh;	//y up line
	float xli = xlo + xwdiff;	//left inner x
	float xro = xlo + fw;	//right outter x
	float xri = xro - xwdiff;	//right inner x
	TCs[0][0][0]=xlo;		//left upper point x
	TCs[0][0][1]=yupline;	// left upper point y

	TCs[0][1][0]=xli;	//left bottom point x
	TCs[0][1][1]=ybase;	//left bottom point y

	TCs[0][2][0]=xro;	//right uppper point x
	TCs[0][2][1]=yupline;	//right upper point y
	
	TCs[0][3][0]=xri;	//right bottom point x
	TCs[0][3][1]=ybase;	//right bottom point y
}

//parameters are texture coordination except last two
void calc_left_params(float lh,float lw,float lho,float lwo,float outterwidth,float innerwidth,float outterheight,float innerheight)
{
	float xright = 1.0f - lwo;	//left line x
	float yih = lh * innerheight / outterheight;	// inner height
	float yhdiff = (lh - yih ) /2 ;	// y diff between inner height and outter height;
	float lwc = (goutterwidth - ginnerwidth)* yhdiff / (goutterheight - ginnerheight);
	float xleft = lwo + lwc / lw;	//right line x;
	float yld = lho;	//left down point y
	float ylu = lho + lh;	//left up point y
	float yrd = lho + yhdiff ;
	float yru = lho + yhdiff + yih;
	TCs[2][0][0] = xleft;	//left upper x
	TCs[2][0][1] = ylu; //left upper y
	
	TCs[2][1][0] = xleft;	// left down x
	TCs[2][1][1] = yld;	//left down y

	TCs[2][2][0] = xright;	// right upper x
	TCs[2][2][1] = yru;	// right upper y

	TCs[2][3][0] = xright;	//right down x
	TCs[2][3][1] = yrd;	// right down y

}

//parameters are texture coordination except last two
void calc_right_params(float rh,float rw,float rho,float rwo,float outterwidth,float innerwidth,float outterheight,float innerheight)
{
	float xleft = rwo;	//left line x
	float yih = rh * innerheight / outterheight;		//inner height 
	float yhdiff = (rh - yih)/2;	// y diff between y inner height and outter height
	float rwc = (goutterwidth - ginnerwidth) * yhdiff / (goutterheight - ginnerheight);
	float xright = rwo + rwc / rw;	//right line x
	float yld = rho + yhdiff;		//left bottom point y
	float ylu = yld + yih;		//left upper point y
	float yrd = rho;			// right down point y
	float yru = yrd + rh;		// right upper point y
	TCs[3][0][0]= xleft;		//left upper point x
	TCs[3][0][1]= ylu;	// left upper point y

	TCs[3][1][0]= xleft;	//left bottom point x
	TCs[3][1][1]= yld;	//left bottom point y

	TCs[3][2][0]= xright;	//right uppper point x
	TCs[3][2][1]= yru;	//right upper point y
	
	TCs[3][3][0]= xright;	//right bottom point x
	TCs[3][3][1]= yrd;	//right bottom point y
}

void calc_back_params(float bh,float bw,float bho,float bwo,float outterwidth,float innerwidth,float outterheight,float innerheight)
{
	float ybase = bho;
	float xlo = bwo;
	float xiw = bw * innerwidth / outterwidth;	//x inner width;
	float xwdiff = (bw - xiw) /2;
	float bhc = (goutterheight - ginnerheight) * xwdiff / (goutterwidth - ginnerwidth);
	float yupline = bho + bhc / bh;
	float xli = xlo + xwdiff; //left inner point x
	float xro = xlo + bw;		//right outter point x
	float xri = xro - xwdiff;	//right inner point x
	TCs[1][0][0] = xlo;		
	TCs[1][0][1] = ybase;

	TCs[1][1][0] = xro;
	TCs[1][1][1] = ybase;

	TCs[1][2][0] = xli;
	TCs[1][2][1] = yupline;

	TCs[1][3][0] = xri;
	TCs[1][3][1] = yupline;
}

/*
GLfloat backTC[4][2]={{0.2f,0.7f},{0.8f,0.7f},{0.3f,0.95},{0.7f,0.95f}};
//GLfloat leftTC[4][2]={{0.7f,0.95f},{0.7f,0.05f},{0.95f,0.7f},{0.95f,0.3f}};	//
GLfloat leftTC[4][2] = {{0.0f,1.0f},{0.0f,0.0f},{1.0f,1.0f},{1.0f,0.0f}};
GLfloat rightTC[4][2]={{0.0f,1.0f},{0.0f,0.0f},{1.0f,1.0f},{1.0f,0.0f}};
GLfloat frontTC[4][2]={{0.05f,0.3f},{0.3f,0.05f},{0.95f,0.3f},{0.7f,0.05f}};
//GLfloat carTC[4][2] = {{0.05f,0.05f},{0.05f,0.95f},{0.05f,0.95f},{0.95f,0.95f}};
//GLfloat carTC[4][2] = {{0.0f,0.0f},{0.0f,1.0f},{0.0f,1.0f},{1.0f,1.0f}};
GLfloat carTC[4][2] = {{0.3f,0.1f},{0.7f,0.1f},{0.3f,0.9f},{0.7f,0.9f}};
*/
#define frontTC TCs[0]
#define backTC TCs[1]
#define leftTC TCs[2]
#define rightTC TCs[3]

// Texture objects
#define TEXTURE_FRONT	0
#define TEXTURE_BACK	1
#define TEXTURE_LEFT	2
#define TEXTURE_RIGHT	3
#define TEXTURE_CAR		4
#define TEXTURE_COUNT   5
GLuint  textures[TEXTURE_COUNT];
//const char *szTextureFiles[TEXTURE_COUNT] = { "brick.tga", "floor.tga", "ceiling.tga" };
const char *szTextureFiles[TEXTURE_COUNT] = { "front.tga",  "back.tga","left.tga","right.tga" ,"car_rect.tga"};



///////////////////////////////////////////////////////////////////////////////
// Change texture filter for each texture object
void ProcessMenu(int value)
	{
    GLint iLoop;
    
    for(iLoop = 0; iLoop < TEXTURE_COUNT; iLoop++)
        {
        glBindTexture(GL_TEXTURE_2D, textures[iLoop]);
        
        switch(value)
            {
            case 0:
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                break;
                
            case 1:
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                break;
                
            case 2:
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
                break;
            
            case 3:
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
                break;
            
            case 4:
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
                break;
                
            case 5:
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
                break;                
            }
        }
        
    // Trigger Redraw
	glutPostRedisplay();
	}



void fourReshape()
{

#define BADF 1
	//WaitForSingleObject( g_hMutex , INFINITE );

	frontBatch.Reset();
    frontBatch.Begin(GL_TRIANGLE_STRIP, 4, 1);
    frontBatch.MultiTexCoord2f(0, frontTC[0][0],frontTC[0][1]);
#if BADF
    frontBatch.Vertex3f(PTs[0][0], PTs[0][1], PTs[0][2]);
#else
	frontBatch.CopyVertexData3f(PTs[0]);
#endif
        
    frontBatch.MultiTexCoord2f(0, frontTC[1][0],frontTC[1][1]);
#if BADF
    frontBatch.Vertex3f(PTs[4][0], PTs[4][1], ZV);
#else
	frontBatch.CopyVertexData3f(PTs[4]);
#endif
        
    frontBatch.MultiTexCoord2f(0, frontTC[2][0],frontTC[2][1]);
#if BADF
    frontBatch.Vertex3f(PTs[1][0],PTs[1][1], ZV);
#else
	frontBatch.CopyVertexData3f(PTs[1]);
#endif

    frontBatch.MultiTexCoord2f(0, frontTC[3][0],frontTC[3][1]);
#if BADF
	frontBatch.Vertex3f(PTs[5][0], PTs[5][1], ZV);
#else
	frontBatch.CopyVertexData3f(PTs[5]);
#endif
    frontBatch.End();
	/*
	printf("front four points  1(%f %f): %f %f\n\t2(%f %f):%f %f\n\t3(%f %f):%f %f\n\t4(%f %f):%f %f\n",
		PTs[0][0], PTs[0][1],frontTC[0][0],frontTC[0][1],
		PTs[4][0], PTs[4][1],frontTC[1][0],frontTC[1][1],
		PTs[1][0],PTs[1][1],frontTC[2][0],frontTC[2][1],
		PTs[5][0], PTs[5][1],frontTC[3][0],frontTC[3][1]);
		*/


	backBatch.Reset();
    backBatch.Begin(GL_TRIANGLE_STRIP, 4, 1);
    backBatch.MultiTexCoord2f(0,backTC[0][0],backTC[0][1]);
#if BADF
    backBatch.Vertex3f(PTs[2][0], PTs[2][1], ZV);
#else
	backBatch.CopyVertexData3f(PTs[2]);
#endif

    backBatch.MultiTexCoord2f(0, backTC[1][0],backTC[1][1]);
#if BADF
    backBatch.Vertex3f(PTs[3][0],PTs[3][1], ZV);
#else
	backBatch.CopyVertexData3f(PTs[3]);
#endif

    backBatch.MultiTexCoord2f(0, backTC[2][0],backTC[2][1]);
#if BADF
	backBatch.Vertex3f(PTs[6][0],PTs[6][1], ZV);
#else
	backBatch.CopyVertexData3f(PTs[6]);
#endif

    backBatch.MultiTexCoord2f(0, backTC[3][0],backTC[3][1]);
    backBatch.Vertex3f(PTs[7][0],PTs[7][1], ZV);
	//backBatch.CopyVertexData3f(PTs[7]);
    backBatch.End();

	leftBatch.Reset();
    leftBatch.Begin(GL_TRIANGLE_STRIP, 4, 1);
    
    leftBatch.MultiTexCoord2f(0, leftTC[0][0],leftTC[0][1]);
    leftBatch.Vertex3f(PTs[0][0], PTs[0][1], ZV);
	//leftBatch.CopyVertexData3f(PTs[0]);
        
    leftBatch.MultiTexCoord2f(0, leftTC[1][0],leftTC[1][1]);
    leftBatch.Vertex3f(PTs[2][0], PTs[2][1], ZV);
	//leftBatch.CopyVertexData3f(PTs[2]);
        
    leftBatch.MultiTexCoord2f(0, leftTC[2][0],leftTC[2][1]);
    leftBatch.Vertex3f(PTs[4][0],PTs[4][1], ZV);
	//leftBatch.CopyVertexData3f(PTs[4]);

    leftBatch.MultiTexCoord2f(0, leftTC[3][0],leftTC[3][1]);
    leftBatch.Vertex3f(PTs[6][0], PTs[6][1], ZV);
	//leftBatch.CopyVertexData3f(PTs[6]);
	leftBatch.End();

	printf("left four points  1(%f %f): %f %f\n\t2(%f %f):%f %f\n\t3(%f %f):%f %f\n\t4(%f %f):%f %f\n",
		PTs[0][0], PTs[0][1],leftTC[0][0],leftTC[0][1],
		PTs[2][0], PTs[2][1],leftTC[1][0],leftTC[1][1],
		PTs[4][0],PTs[4][1],leftTC[2][0],leftTC[2][1],
		PTs[6][0], PTs[6][1],leftTC[3][0],leftTC[3][1]);

	rightBatch.Reset();
    rightBatch.Begin(GL_TRIANGLE_STRIP, 4, 1);
    
    rightBatch.MultiTexCoord2f(0, rightTC[0][0],rightTC[0][1]);
    rightBatch.Vertex3f(PTs[5][0], PTs[5][1], ZV);
	//rightBatch.CopyVertexData3f(PTs[5]);
        
    rightBatch.MultiTexCoord2f(0, rightTC[1][0],rightTC[1][1]);
    rightBatch.Vertex3f(PTs[7][0],PTs[7][1], ZV);
	//rightBatch.CopyVertexData3f(PTs[7]);
        
    rightBatch.MultiTexCoord2f(0, rightTC[2][0],rightTC[2][1]);
    rightBatch.Vertex3f(PTs[1][0],PTs[1][1], ZV);
	//rightBatch.CopyVertexData3f(PTs[1]);

    rightBatch.MultiTexCoord2f(0, rightTC[3][0],rightTC[3][1]);
    rightBatch.Vertex3f(PTs[3][0],PTs[3][1] , ZV);
	//rightBatch.CopyVertexData3f(PTs[3]);
    rightBatch.End();

	/*
	printf("right four points  1(%f %f): %f %f\n\t2(%f %f):%f %f\n\t3(%f %f):%f %f\n\t4(%f %f):%f %f\n",
		PTs[5][0], PTs[5][1],rightTC[0][0],rightTC[0][1],
		PTs[7][0], PTs[7][1],rightTC[1][0],rightTC[1][1],
		PTs[1][0],PTs[1][1],rightTC[2][0],rightTC[2][1],
		PTs[3][0], PTs[3][1],rightTC[3][0],rightTC[3][1]);
		*/

	carBatch.Reset();
	carBatch.Begin(GL_TRIANGLE_STRIP,4,1);
	carBatch.MultiTexCoord2f(0, carTC[0][0],carTC[0][1]);
    carBatch.Vertex3f(PTs[6][0],PTs[6][1] , ZV);

	carBatch.MultiTexCoord2f(0, carTC[1][0],carTC[1][1]);
    carBatch.Vertex3f(PTs[7][0],PTs[7][1] , ZV);

	carBatch.MultiTexCoord2f(0, carTC[2][0],carTC[2][1]);
    carBatch.Vertex3f(PTs[4][0],PTs[4][1] , ZV);

	carBatch.MultiTexCoord2f(0, carTC[3][0],carTC[3][1]);
    carBatch.Vertex3f(PTs[5][0],PTs[5][1] , ZV);
    
	carBatch.End();

	ReleaseMutex(g_hMutex);

}

//////////////////////////////////////////////////////////////////
// This function does any needed initialization on the rendering
// context.  Here it sets up and initializes the texture objects.
void SetupRC()
    {
    GLbyte *pBytes;
    GLint iWidth, iHeight, iComponents;
    GLenum eFormat;
    GLint iLoop;
    
	g_hMutex = CreateMutex( NULL , FALSE , NULL );
	// Black background
	glClearColor(0.0f, 0.0f, 0.0f,1.0f);
    glEnable(GL_CULL_FACE);
	 glFrontFace(GL_FRONT);
    shaderManager.InitializeStockShaders();

    // Load textures
    glGenTextures(TEXTURE_COUNT, textures);
    for(iLoop = 0; iLoop < TEXTURE_COUNT; iLoop++)
    {
        // Bind to next texture object
        glBindTexture(GL_TEXTURE_2D, textures[iLoop]);
        
        // Load texture, set filter and wrap modes
        pBytes = gltReadTGABits(szTextureFiles[iLoop],&iWidth, &iHeight,
                              &iComponents, &eFormat);

        // Load texture, set filter and wrap modes
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexImage2D(GL_TEXTURE_2D, 0, iComponents, iWidth, iHeight, 0, eFormat, GL_UNSIGNED_BYTE, pBytes);
		printf("texture %d width %d height %d format %d\n",iLoop,iWidth,iHeight);
        glGenerateMipmap(GL_TEXTURE_2D);
        // Don't need original texture data any more
        free(pBytes);
    }
        
    // Build Geometry
    //GLfloat z;
	fourReshape();

	
    }
    
///////////////////////////////////////////////////
// Shutdown the rendering context. Just deletes the
// texture objects
void ShutdownRC(void)
    {
    glDeleteTextures(TEXTURE_COUNT, textures);
    }
    
void KeyFunc(unsigned char key ,int x,int y){
	static int index1=0,index2=0;
	static GLfloat step = 0.1f;
	switch(key){
	case '1':
	case '2':
	case '3':
	case '4':
		index1 = key - '1';
		break;
	case 'x':
		index2=0;
		break;
	case 'y':
		index2=1;
		break;
	case 'a':
		leftTC[index1][0]-=step;
		break;
	case 's':
		leftTC[index1][1]-=step;
		break;
	case 'd':
		leftTC[index1][0]+=step;
		break;
	case 'w':
		leftTC[index1][1]+=step;
		break;
	default:
		break;
	}
	fourReshape();
	glutPostRedisplay();
}


void postDisplay()
{
	needReshape = true;
	glutPostRedisplay();
}
///////////////////////////////////////////////////
// Respond to arrow keys, move the viewpoint back
// and forth
void SpecialKeys(int key, int x, int y)
	{
		
	if(key == GLUT_KEY_UP)
        viewZ += 0.5f;

	if(key == GLUT_KEY_DOWN)
        viewZ -= 0.5f;

	
	printf("viewZ is %f\n",viewZ);
	// Refresh the Window
	glutPostRedisplay();
	}

/////////////////////////////////////////////////////////////////////
// Change viewing volume and viewport.  Called when window is resized
void ChangeSize(int w, int h)
    {
    GLfloat fAspect;

    // Prevent a divide by zero
    if(h == 0)
        h = 1;
	printf("width %d height %d\n",w,h);
    // Set Viewport to window dimensions
    glViewport(0, 0, w, h);

    fAspect = (GLfloat)w/(GLfloat)h;

    // Produce the perspective projection
	viewFrustum.SetPerspective(80.0f,fAspect,1.0,120.0);
    projectionMatrix.LoadMatrix(viewFrustum.GetProjectionMatrix());
    transformPipeline.SetMatrixStacks(modelViewMatrix, projectionMatrix);

    }

///////////////////////////////////////////////////////
// Called to draw scene
void RenderScene(void)
    {
		//WaitForSingleObject(g_hMutex,INFINITE);
		if(needReshape){
			fourReshape();
			needReshape = false;
		}
    // Clear the window with current clearing color
    glClear(GL_COLOR_BUFFER_BIT);

    modelViewMatrix.PushMatrix();
        modelViewMatrix.Translate(0.0f, 0.0f, viewZ);
        
        shaderManager.UseStockShader(GLT_SHADER_TEXTURE_REPLACE, transformPipeline.GetModelViewProjectionMatrix(), 0);
		if(gBackShow){
			glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_BACK]);
			backBatch.Draw();
		}
        
		if(gFrontShow){
			glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_FRONT]);
			frontBatch.Draw();
		}

		if(gLeftShow){
			glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_LEFT]);
			leftBatch.Draw();
		}
		
		if(gRightShow){
			glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_RIGHT]);
			rightBatch.Draw();
		}
		glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_CAR]);
		carBatch.Draw();
        
    modelViewMatrix.PopMatrix();

    // Buffer swap
    glutSwapBuffers();
	ReleaseMutex(g_hMutex);
    }


//////////////////////////////////////////////////////
// Program entry point
int main(int argc, char *argv[])
    {
    gltSetWorkingDirectory(argv[0]);

    // Standard initialization stuff
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(600, 600);
    glutCreateWindow("Tunnel");
    glutReshapeFunc(ChangeSize);
    glutSpecialFunc(SpecialKeys);
	glutKeyboardFunc(KeyFunc);
    glutDisplayFunc(RenderScene);
    
    // Add menu entries to change filter
    glutCreateMenu(ProcessMenu);
    glutAddMenuEntry("GL_NEAREST",0);
    glutAddMenuEntry("GL_LINEAR",1);
    glutAddMenuEntry("GL_NEAREST_MIPMAP_NEAREST",2);
    glutAddMenuEntry("GL_NEAREST_MIPMAP_LINEAR", 3);
    glutAddMenuEntry("GL_LINEAR_MIPMAP_NEAREST", 4);
    glutAddMenuEntry("GL_LINEAR_MIPMAP_LINEAR", 5);

    glutAttachMenu(GLUT_RIGHT_BUTTON);
    
    GLenum err = glewInit();
    if (GLEW_OK != err) {
        fprintf(stderr, "GLEW Error: %s\n", glewGetErrorString(err));
        return 1;
    }
        
    void mavudp_init(void);
	mavudp_init();
    // Startup, loop, shutdown
    SetupRC();
    glutMainLoop();
	void mavudp_exit(void);
	mavudp_exit();
    ShutdownRC();
    
    return 0;
    }
    
    
  
